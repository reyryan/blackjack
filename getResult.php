<?php
require_once "card.php";

$score = 0;
foreach($_POST['cards'] as $card) {
	$face = $card['face'];
	$suit = $card['suit'];
	
	echo "$face$suit<br/>";
	
	$oCard = new Card($face, $suit);
	$score += $oCard->getValue();
}

echo "SCORE: $score";