<?php
class Card {
	protected $face;
	protected $suit;
	
	public function __construct($face, $suit) {
		$this->face = $face;
		$this->suit = $suit;
	}
	
	public function getFace() {
		return $this->face;
	}
	
	public function getSuit() {
		return $this->suit;
	}
	
	public function getValue() {
		return $this->face == 'A' ? 11 : (in_array($this->face,array('J','Q','K')) ? 10 : $this->face);
	}
}