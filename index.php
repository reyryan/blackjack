<html>
<head><title>BlackJack</title>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>

<script type="text/javascript">
function createCards(howMany) {
	for(x = 0; x<howMany; x++) {
		var faces = [
			{val: '2', txt: '2'},
			{val: '3', txt: '3'},
			{val: '4', txt: '4'},
			{val: '5', txt: '5'},
			{val: '6', txt: '6'},
			{val: '7', txt: '7'},
			{val: '8', txt: '8'},
			{val: '9', txt: '9'},
			{val: '10', txt: '10'},
			{val: 'J', txt: 'Jack'},
			{val: 'Q', txt: 'Queen'},
			{val: 'K', txt: 'King'},
			{val: 'A', txt: 'Ace'}
		],
		suits = [
			{val: 'S', txt: 'Spades'},
			{val: 'C', txt: 'Clubs'},
			{val: 'D', txt: 'Diamonds'},
			{val: 'H', txt: 'Hearts'}
		],
		lblText = 'Card' + (x+1) + ': ';
		face = $('<select id="card' + (x+1) + '" name="cards[' + x + '][face]">').insertBefore($('#reset')),
		suit = $('<select id="suit' + (x+1) + '" name="cards[' + x + '][suit]">').insertAfter(face).after('<br>'),
		label = $('<label>').text(lblText).insertBefore(face);
		
		$(faces).each(function() {
			face.append($("<option>").attr('value',this.val).text(this.txt));
		});
		
		$(suits).each(function() {
			suit.append($("<option>").attr('value',this.val).text(this.txt));
		});
	}
}
</script>
</head>

<body onLoad = 'createCards(2);'>
<form method="POST" action="getResult.php">
<input id="reset" type="reset" value="Reset"/>
<input id="submit" type="submit" value="Submit"/>
</form>
</body>
</html>